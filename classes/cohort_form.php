<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "Profile field based cohort membership" - Form for selecting which cohorts this plugin should manage.
 *
 * @package   local_selfcohort
 * @copyright 2016 Davo Smith, Synergy Learning UK on behalf of Alexander Bias, Ulm University <alexander.bias@uni-ulm.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_selfcohort;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir.'/formslib.php');

/**
 * Class cohort_form
 * @package   local_selfcohort
 * @copyright 2016 Davo Smith, Synergy Learning UK on behalf of Alexander Bias, Ulm University <alexander.bias@uni-ulm.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cohort_form extends \moodleform {

    /**
     * Form definition.
     */
    protected function definition() {
        $mform = $this->_form;
        $cohorts = $this->_customdata['cohorts'];

        $registerlink = (new \moodle_url('/local/selfcohort/register.php'))->out();
        $intro = '<p>' . get_string('cohortsintro', 'local_selfcohort') . '</p>' .
                 '<p>' . get_string('invisiblecohortsnote', 'local_selfcohort') . '</p>' .
                 '<p>' . get_string('selfregisterlink', 'local_selfcohort', $registerlink) . '</p>' .
                 '<p>' . get_string('selfregisterintrostring', 'local_selfcohort', $registerlink) . '</p>';
        $mform->addElement('html', \html_writer::tag('div', $intro, ['id' => 'intro', 'class' => 'box generalbox']));

        if (!$cohorts) {
            $cohorturl = new \moodle_url('/cohort/index.php');
            $link = \html_writer::link($cohorturl, get_string('cohorts', 'core_cohort'));
            $mform->addElement('html', \html_writer::tag('div', get_string('nocohorts', 'local_selfcohort', $link),
                                                         ['class' => 'alert alert-warning']));
        } else {
            $options = [];
            foreach ($cohorts as $cohort) {
                $options[] = $mform->createElement('advcheckbox', $cohort->id, null, format_string($cohort->name));
                $mform->setDefault("cohort[$cohort->id]", ($cohort->component == 'local_selfcohort'));
            }
            $mform->addGroup($options, 'cohort', get_string('availablecohorts', 'local_selfcohort'));
        }

        $this->add_action_buttons();
    }
}
