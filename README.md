# Moodle Plugin - Self cohort #

This plugins allow users to self register themselves as cohort members to a list of cohorts pre-selected by the site administrators.

With this plugins users will be able to add or remove themselves to/from cohorts.
The message on the registering page can be customized via language customization.
This can be used, for example, with Learning Plan Templates that are linked to cohorts so when a user adds himself/herself to the cohort a learning plan is automatic created for him/her.

## License ##

2020 Daniel Neis Araujo <daniel@adapta.online>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
